package cp437tree

import (
	"testing"

	"github.com/stretchr/testify/assert"
)


func Test_Main(t *testing.T) {
	tree :=  Tree{
		Text: "1",
		Children: []Tree{
			{
				Text: "2",
				Children: []Tree{
					{
						Text: "3",
					},
					{
						Text: "4",
					},
					{
						Text: "5",
					},
					{
						Text: "6",
					},
					{
						Text: "7",
						Children: []Tree{
							{
								Text: "8",
							},
						},
					},
				},
			},
			{
				Text: "9",
			},
			{
				Text: "10",
				Children: []Tree{
					{
						Text: "11",
						Children: []Tree{
							{
								Text: "12",
								Children: []Tree{
									{
										Text: "13",
										Children: []Tree{
											{
												Text: "14",
												Children: []Tree{
													{
														Text: "15",
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}

	assert.Equal(t, tree.Text, "1")
	assert.Equal(t, tree.Children[0].Text, "2")
	assert.Equal(t, tree.Children[2].Children[0].Children[0].Children[0].Children[0].Children[0].Text, "15")
}